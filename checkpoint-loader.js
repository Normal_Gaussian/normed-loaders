const checkpoints = {};
module.exports = function(source, map, meta) {
  if(checkpoints[this.resource]) {
    let args = checkpoints[this.resource];
    this.callback.apply(this.callback, args);
    delete checkpoints[this.resource];
    return;
  } else {
    checkpoints[this.resource] = [null, source, map, meta];
    this.callback(null, source, map, meta);
    return;
  }
}