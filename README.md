# Loaders

A collection of loaders made to use with norm-build

## checkpoint-loader

Invoke with no options and it "saves" the current state for this resource. Invoke again and it "reverts" to that save.

This is technically stateful, but stateless in all loaders, and is useful to revert changes another loader makes.

This might be:
- you want to use the checks but not the transformations of a loader.
- you want the side effects of a loader.

The second case shouldn't occur, but it does. norm-build uses checkpoint-loader to revert the transpilation of TypeScript files whilst preserving the generated definitions files, thus allowing compilation to be done by the significantly more sane babel.

## ruin-loader

Replaces source with the word "ruin". Useful as a test of the checkpoint-loader.

## resolve-loader

Apply webpack resolve mechanisms to a file *before* it goes through other loaders that do not do it.

This is represents an issue with those loaders, and a workaround.

Currently only supports js style imports, and uses a regex method instead of an AST, so may be incorrect (please report issues).