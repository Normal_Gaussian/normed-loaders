const getOptions = require('loader-utils').getOptions;
const validateOptions = require('schema-utils');

// Apply webpack resolve mechanisms to a file *before* it goes through other loaders
//  that do not do it.
// This represents an issue with those loaders, and a workaround.
const path = require('path');

const builtins = Object.keys(process.binding('natives'))
    .filter(m => !m.startsWith("internal"))
    .filter(m => !m.startsWith('_'))

function resolve(context, path, expect) {
    return new Promise((pass, fail) => {
        if(expect.includes(path)) {
            pass(path)
        } else {
            context.resolve(context.context, path, (err, result) => {
                err ? fail(err) : pass(result)
            })
        }
    })
}
 
async function getExtensionsFor(context, inputpath, expect) {
    // To resolve the extensions only, and not get the fully resolved path we must
    //  * fully resolve the path
    //  * compare basenames, if the input basename is not a substr starting at 0 of the
    //     fully resolved basename return the input
    //  * the difference between basenames is the "extension"
    //  * if the input plus extension does not resolve to the fully resolved path, then
    //     return the input
    //  * otherwise, return input plus extension

    const resolvedpath = await resolve(context, inputpath, expect);
    const inputBasename = path.basename(inputpath);
    const resolvedBasename = path.basename(resolvedpath);
    if(resolvedBasename.indexOf(inputBasename) !== 0) {
        return inputpath;
    }
    const extension = resolvedBasename.slice(inputBasename.length);
    if(extension.length == 0) {
        return inputpath;
    } 
    const extendedpath = inputpath + extension;
    try {
        const resolvedextendedpath = await resolve(context, extendedpath);
        if(resolvedextendedpath !== resolvedpath) {
            return inputpath;
        }
    } catch(e) {
        return inputpath;
    }
    return extendedpath;
}

const schema = {
    type: 'object',
    properties: {
        node: {
            type: 'boolean'
        }
    },
    additionalProperties: false
}

module.exports = function(source) {
    const options = getOptions(this)
    validateOptions(schema, options, 'Resolve-Loader')

    let expect = options.node ? builtins : [];

    // Gather all the import statements
    const importregex = /(^|\n)\s*import\s.*?\sfrom\s+("([^"\n]+)"|'([^'\n]+)')/g;
 
    const imports = [];
    let j;
    while((j = importregex.exec(source)) !== null) {
        imports.push(j);
    }


    // Creates a list of promises that resolve to sections of the file
    // each section is either a "plain" section that does not contain
    // an import, or an "import" section.
    // They are promises to facilitate resolving the imports
    // Plain sections are found between imports
    let promises = [];
    let pos = 0;
    for(let i=0; i<imports.length; i++) {
        match = imports[i];
        
        let internalMatch = match[3] || match[4];               // the content of from "<x>"
        let internalOffset = match[0].indexOf(match[2]) + 1;    // offset of <x>
        let matchLength = match[0].length;
        let start = match.index + internalOffset;
        let delcount = internalMatch.length;

        // The section between the start of the file or the last import and the content of this import (plain)
        promises.push((async (start, end) => source.slice(start, end))(pos, start))
    
        // This import
        promises.push((async (internalMatch) => {
            let insert = internalMatch;
            try {
                insert = await getExtensionsFor(this, internalMatch, expect);
            } catch(e) {
                this.emitError(`resolve-loader couldn't resolve "${internalMatch}"`, e)
            }
            return insert;
        })(internalMatch))

        // The section after the import to the end of the match (plain)
        promises.push((async (start, end) => source.slice(start, end))(start + delcount, match.index + matchLength))

        pos = match.index + matchLength;
    }

    // from the last import to the end of the file (plain)
    promises.push((async (pos) => source.slice(pos))(pos))

    // Put the sections of the file back together
    //   and output it
    Promise.all(promises).then(results => {
        const output = results.join('');
        this.callback(null, output);
    });

    this.async()
}